//Passed variables
var translatable = translations;

//Image slider
(function($) {

	$(document).ready(function() {

		var owl = $('.housing-company-images-slider');
		owl.addClass('owl-carousel').owlCarousel({
			dots: false,
			autoplay: false,
			loop: false,
			navSpeed: 750,
			dragEndSpeed: 500,
			lazyLoad: true,
			margin: 0,
			items: 1,
			navElement: 'div',
			navContainerClass: 'inner-control',
			navText: ['<button class="prev-arrow" aria-label="' + translatable.sliderprev + '"></button>','<button class="next-arrow" aria-label="' + translatable.slidernext + '"></button>'],
			nav: $('.housing-company-image').length > 1 ? true : false,
			onRefresh: function () {
				owl.find('div.owl-item').height('');
			},
			onRefreshed: function () {
				var width = owl.find('div.owl-item').outerWidth();
				var newHeight = width * 0.66;
				owl.find('div.owl-item').height(newHeight);
			}
		});

		$( '.prev-arrow' ).mouseleave(function() {
			$(this).blur();
		});

		$( '.next-arrow' ).mouseleave(function() {
			$(this).blur();
		});

		$('.house-image').each(function() {
			$(this).magnificPopup({
				type: 'image',
				delegate: 'a',
				mainClass: 'mfp-img-mobile',
				image: {
					verticalFit: true
				},
				gallery: {
					enabled: true,
					navigateByImgClick: true,
					preload: [0,1],
					tCounter: '%curr% / %total%'
				},
			});
		});

		$('.video-popup').magnificPopup({
			type: 'iframe',
			mainClass: 'mfp-fade',
			iframe: {
				patterns: {
					youtu: {
					index: 'youtu.be',
					id: function( url ) {

						// Capture everything after the hostname, excluding possible querystrings.
						var m = url.match( /^.+youtu.be\/([^?]+)/ );

						if ( null !== m ) {
							return m[1];
						}

						return null;

					},
					// Use the captured video ID in an embed URL.
					// Add/remove querystrings as desired.
					src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0'
					}
				}
			}
		});

	});

})(jQuery);

