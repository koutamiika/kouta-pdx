<?php
/**
 * Housing companies html
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Pdx
 * @subpackage Kouta_Pdx/public/partials
 */

?>

<div class="pdx-housing-company">
	<div class="housing-company-images">
		<div class="housing-company-images-slider">
			<?php foreach ( $main->pictures->picture as $picture ) : ?>
				<div class="housing-company-image">
					<img src="<?php echo esc_url( $picture ); ?>" loading="lazy">
				</div><!-- .house-image -->
			<?php endforeach; ?>
		</div><!-- .housing-company-images-slider -->
	</div><!-- .housing-company-images -->

	<div class="housing-company-info">
		<h2><?php echo esc_html( $main->name ); ?></h2>
		<p><?php echo wp_kses_post( $main->{'presentation-text'} ); ?></p>

		<ul>
			<li><?php echo esc_html( $main->{'construction-details'}->{'construction-company-name'} ); ?></li>
			<li><?php esc_html_e( 'Arvioitu valmiustumisaika:', 'kouta-pdx' ); ?> <?php echo esc_html( $main->{'construction-details'}->{'estimated-completion-time'} ); ?></li>
		</ul>

		<h3><?php esc_html_e( 'Esittely', 'kouta-pdx' ); ?></h3>
		<ul>
			<?php foreach ( $main->{'property-development'}->{'virtual-presentations'}->{'virtual-presentation'} as $presentation ) : ?>
				<?php foreach ( $presentation->attributes() as $attribute ) : ?>

					<?php if ( strpos( $attribute, 'https' ) !== false ) : ?>
						<li><a href="<?php echo esc_url( $attribute ); ?>" class="video-popup"><?php echo esc_html( $presentation ); ?></a></li>
					<?php else : ?>
						<li><a href="<?php echo esc_url( $attribute ); ?>" target="_blank" rel="noopener nofollow"><?php echo esc_html( $presentation ); ?></a></li>
					<?php endif; ?>

				<?php endforeach; ?>
			<?php endforeach; ?>

			<?php foreach ( $main->{'property-development'}->{'more-info'} as $presentation ) : ?>
				<?php foreach ( $presentation->attributes() as $attribute ) : ?>
					<?php $attr = (string) $attribute; ?>
					<?php if ( ! empty( $attr ) ) : ?>
						<li><a href="<?php echo esc_url( $attr ); ?>" target="_blank" download rel="nofollow"><?php esc_html_e( 'Lataa esite', 'kouta-pdx' ); ?></a></li>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endforeach; ?>
		</ul>

		<h3><?php esc_html_e( 'Sijainti', 'kouta-pdx' ); ?></h3>
		<ul>
			<li><?php echo esc_html( $main->address->street ); ?></li>
			<li><?php echo esc_html( $main->address->{'postal-code'} ); ?> <?php echo esc_html( $main->address->region ); ?></li>
		</ul>

		<div class="housing-contact">
			<h3><?php esc_html_e( 'Kysy lisää', 'kouta-pdx' ); ?></h3>
			<p><?php echo esc_html( $main->estate_agent_contact_person_name ); ?></p>
			<p><a href="tel:<?php echo esc_attr( $main->estate_agent_contact_person_telephone ); ?>"><?php echo esc_html( $main->estate_agent_contact_person_telephone ); ?></a></p>
			<p><a href="mailto:<?php echo esc_attr( $main->estate_agent_contact_person_email ); ?>"><?php echo esc_html( $main->estate_agent_contact_person_email ); ?></a></p>
		</div><!-- .housing-contact -->

	</div><!-- .housing-company-info -->
</div><!-- .housing-company -->