<?php

/**
 * House HTML
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Pdx
 * @subpackage Kouta_Pdx/public/partials
 */
?>

<div class="pdx-house">

	<div class="house-image">

		<a href="<?php echo esc_url( $house->Large1 ); ?>" data-elementor-open-lightbox="no">
			<img src="<?php echo esc_url( $house->Thumbbig1 ); ?>" loading="lazy">
		</a>

		<?php
		for ( $i = 2; $i <= 20; $i++ ) {
			$picture = 'Large' . $i;

			if ( ! empty( $house->$picture ) ) {
				echo '<a href="' . esc_url( $house->$picture ) . '" tabindex="-1" role="button" aria-disabled="true" data-elementor-open-lightbox="no"></a>';
			} else {
				break;
			}
		}
		?>
	</div><!-- .house-image -->

	<div class="house-text">

		<h3><?php echo esc_html( $house->StreetAddress ); ?></h3>

		<ul>
			<li><strong><?php esc_html_e( 'Pinta-ala:', 'kouta-pdx' ); ?></strong> <?php echo esc_html( $house->LivingArea ); ?>m<sup>2</sup></li>
			<li><strong><?php esc_html_e( 'Huoneet:', 'kouta-pdx' ); ?></strong> <?php echo esc_html( $house->RoomTypes ); ?></li>
			<?php if ( ! empty( $house->DirectionOfWindows ) ) : ?>
			<li><strong><?php esc_html_e( 'Ikkunoiden suunta:', 'kouta-pdx' ); ?></strong></br><?php echo esc_html( $house->DirectionOfWindows ); ?></li>
			<?php endif; ?>
		</ul>

		<ul>
			<?php if ( ! empty( $house->VirtualPresentation ) ) : ?>

				<?php if ( strpos( $house->VirtualPresentation, 'https' ) !== false ) : ?>
					<li><a href="<?php echo esc_url( $house->VirtualPresentation ); ?>" class="video-popup"><strong><?php esc_html_e( 'Virtuaaliesittely', 'kouta-pdx' ); ?></strong></a></li>
				<?php else : ?>
					<li><a href="<?php echo esc_url( $house->VirtualPresentation ); ?>" target="_blank" rel="noopener nofollow"><strong><?php esc_html_e( 'Virtuaaliesittely', 'kouta-pdx' ); ?></strong></a></li>
				<?php endif; ?>

			<?php endif; ?>

			<?php if ( ! empty( $house->VideoClip ) ) : ?>

				<?php if ( strpos( $house->VideoClip, 'https' ) !== false ) : ?>
					<li><a href="<?php echo esc_url( $house->VideoClip ); ?>" class="video-popup"><strong><?php esc_html_e( 'Videoesittely', 'kouta-pdx' ); ?></strong></a></li>
				<?php else : ?>
					<li><a href="<?php echo esc_url( $house->VideoClip ); ?>" target="_blank" rel="noopener nofollow"><strong><?php esc_html_e( 'Videoesittely', 'kouta-pdx' ); ?></strong></a></li>
				<?php endif; ?>

			<?php endif; ?>
		</ul>

		<?php
		$sale_price         = str_replace( '.00', '', $house->SalesPrice );
		$unencumbered_price = str_replace( '.00', '', $house->UnencumberedSalesPrice );
		?>

		<ul>
			<li><strong><?php esc_html_e( 'Saatavilla:', 'kouta-pdx' ); ?></strong> <?php echo esc_html( $house->DateWhenAvailable ); ?></li>
			<li><strong><?php esc_html_e( 'Myyntihinta:', 'kouta-pdx' ); ?></strong>  <?php echo esc_html( number_format( $sale_price, 0, ',', ' ' ) ); ?> €</li>
			<li><strong><?php esc_html_e( 'Velaton myyntihinta:', 'kouta-pdx' ); ?></strong> <?php echo esc_html( number_format( $unencumbered_price, 0, ',', ' ' ) ); ?> €</li>
		</ul>
	</div><!-- .house-text -->

</div><!-- .house -->