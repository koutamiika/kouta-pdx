<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Pdx
 * @subpackage Kouta_Pdx/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version.
 *
 * @package    Kouta_Pdx
 * @subpackage Kouta_Pdx/public
 * @author     Kouta Media <miika@koutamedia.fi>
 */
class Kouta_Pdx_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $plugin_name       The name of the plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/kouta-pdx-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'magnific-styles', plugin_dir_url( __FILE__ ) . 'css/magnific-popup.css', array(), $this->version );
		wp_enqueue_style( 'owl-styles', plugin_dir_url( __FILE__ ) . 'css/owl.carousel.min.css', array(), $this->version );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( 'magnific-popup', plugin_dir_url( __FILE__ ) . 'js/magnific-popup.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/kouta-pdx-public.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'owl-scripts', plugin_dir_url( __FILE__ ) . 'js/owl.carousel.min.js', array( 'jquery' ), $this->version, true );
		$translations = array(
			'slidernext' => __( 'Seuraava', 'kouta-pdx' ),
			'sliderprev' => __( 'Edellinen', 'kouta-pdx' ),
		);
		wp_localize_script( $this->plugin_name, 'translations', $translations );

	}

}
