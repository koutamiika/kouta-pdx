<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Pdx
 * @subpackage Kouta_Pdx/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Kouta_Pdx
 * @subpackage Kouta_Pdx/admin
 * @author     Kouta Media <miika@koutamedia.fi>
 */
class Kouta_Pdx_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $plugin_name       The name of this plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Register Kouta PDX plugins menu
	 *
	 * @since     1.0.0
	 */
	public function register_menu_page() {
		add_submenu_page(
			'options-general.php',
			'Kouta PDX',
			'Kouta PDX',
			'manage_options',
			$this->plugin_name,
			array( $this, 'load_admin_page_content' ), // Calls function to require the partial.
			82
		);
	}

	/**
	 * Register settings for the plugin
	 */
	public function register_settings() {
		register_setting( 'kouta-pdx-settings', 'kouta-pdx-remote-url' );
		register_setting( 'kouta-pdx-settings', 'kouta-pdx-hc-remote-url' );
	}

	public function add_action_links( $links ) {
		$mylinks = array(
			'<a href="' . admin_url( 'options-general.php?page=kouta-pdx' ) . '">' . __( 'Asetukset', 'kouta-pdx' ) . '</a>',
		);
		return array_merge( $links, $mylinks );
	}

	/**
	 * Load admin menu view
	 */
	public function load_admin_page_content() {
		include_once KOUTA_PDX_BASE_DIR . '/admin/partials/kouta-pdx-settings-view.php';
	}

}
