<?php
/**
 * Provide a admin area view for the plugin
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 * @package    Kouta_Pdx
 * @subpackage Kouta_Pdx/admin/partials
 */

?>

<div class="wrap">

	<h1><?php esc_html_e( 'Kouta PDX Asetukset', 'kouta-pdx' ); ?></h1>

	<form method="post" action="options.php">

		<?php settings_fields( 'kouta-pdx-settings' ); ?>
		<?php do_settings_sections( 'kouta-pdx-settings-section' ); ?>

		<section class="postbox">
			<div class="inside">
				<header><h2><?php esc_html_e( 'Kohteet', 'kouta-pdx' ); ?></h2></header>

				<table class="form-table">
					<tr valign="top">
						<th scope="row"><?php esc_html_e( 'Kohteiden PDX osoite', 'kouta-pdx' ); ?></th>
						<td><input type="url" class="widefat" name="kouta-pdx-remote-url" value="<?php echo esc_attr( get_option( 'kouta-pdx-remote-url' ) ); ?>" placeholder="<?php echo esc_attr__( 'esim. https://intra.pdx.fi/yrityksen_nimi/save/out/kotisivut.xml', 'kouta-pdx' ); ?>" /></td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php esc_html_e( 'Lyhytkoodi', 'kouta-pdx' ); ?></th>
						<td>
							<code>[pdx_apartments]</code>
							<p><strong><?php esc_html_e( 'Parametrit:', 'kouta-pdx' ); ?></strong></p>
							<p><?php esc_html_e( 'type: Kohteen tyyppi. Esim LT (liiketila), ET (erillistalo) tai KT. Hyväksyy useita tyyppejä.', 'kouta-pdx' ); ?> <code>[pdx_apartments type="ET, KT"]</code></p>
							<p><?php esc_html_e( 'show_reserved: Näyttää / Piilottaa varatut kohteet. Arvot true/false. Oletuksena false.', 'kouta-pdx' ); ?> <code>[pdx_apartments show_reserved="false"]</code></p>
						</td>
					</tr>
				</table>
			</div>
		</section>

		<section class="postbox">
			<div class="inside">
				<header><h2><?php esc_html_e( 'Taloyhtiöt', 'kouta-pdx' ); ?></h2></header>

				<table class="form-table">
					<tr valign="top">
						<th scope="row"><?php esc_html_e( 'Taloyhtiöiden PDX osoite', 'kouta-pdx' ); ?></th>
						<td><input type="url" class="widefat" name="kouta-pdx-hc-remote-url" value="<?php echo esc_attr( get_option( 'kouta-pdx-hc-remote-url' ) ); ?>" placeholder="<?php echo esc_attr__( 'esim. https://intra.pdx.fi/yrityksen_nimi/save/out/kotisivut-rc.xml', 'kouta-pdx' ); ?>" /></td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php esc_html_e( 'Lyhytkoodi', 'kouta-pdx' ); ?></th>
						<td><code>[pdx_housing_companies]</code></td>
					</tr>
				</table>
			</div>
		</section>

		<?php submit_button(); ?>

	</form>

</div>
