<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://koutamedia.fi
 * @since             1.0.0
 * @package           Kouta_Pdx
 *
 * @wordpress-plugin
 * Plugin Name:       Kouta PDX
 * Plugin URI:        https://koutamedia.fi
 * Description:       Tuo myytävät ja vuokrattavat kohteet kotisivuillesi PDX+ -järjestelmästä.
 * Version:           1.3.0
 * Author:            Kouta Media
 * Author URI:        https://koutamedia.fi
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       kouta-pdx
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'KOUTA_PDX_VERSION', '1.3.0' );

/**
 * Plugin base dir path.
 * used to locate plugin resources primarily code files
 */
define( 'KOUTA_PDX_BASE_DIR', plugin_dir_path( __FILE__ ) );
define( 'KOUTA_PDX_BASENAME', plugin_basename( __FILE__ ) );

/**
 * Check for updates.
 */

require KOUTA_PDX_BASE_DIR . '/puc/plugin-update-checker.php';
$update_checker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/koutamiika/kouta-pdx',
	__FILE__,
	'kouta-pdx'
);

$update_checker->setAuthentication(
	array(
		'consumer_key'    => 'jfqp98eLNSPvrQRRJJ',
		'consumer_secret' => 'SMjuZ6FSqdEB2wBC7mS2DXKgaev9eHA9',
	)
);

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-kouta-pdx-activator.php
 */
function activate_kouta_pdx() {
	require_once KOUTA_PDX_BASE_DIR . 'includes/class-kouta-pdx-activator.php';
	Kouta_Pdx_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-kouta-pdx-deactivator.php
 */
function deactivate_kouta_pdx() {
	require_once KOUTA_PDX_BASE_DIR . 'includes/class-kouta-pdx-deactivator.php';
	Kouta_Pdx_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_kouta_pdx' );
register_deactivation_hook( __FILE__, 'deactivate_kouta_pdx' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require KOUTA_PDX_BASE_DIR . 'includes/class-kouta-pdx.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_kouta_pdx() {

	$plugin = new Kouta_Pdx();
	$plugin->run();

}
run_kouta_pdx();
