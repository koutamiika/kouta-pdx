<?php
/**
 * The Apartments functionality of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Pdx
 * @subpackage Kouta_Pdx/includes
 */

/**
 * The Apartments functionality of the plugin.
 *
 * @package    Kouta_Pdx
 * @subpackage Kouta_Pdx/includes
 * @author     Kouta Media <miika@koutamedia.fi>
 */
class Kouta_Pdx_Apartments {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $plugin_name       The name of the plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	private function fetch_apartments( $url ) {

		if ( ! $url ) {
			return;
		}

		$response = wp_remote_get(
			$url
		);

		$response_body = wp_remote_retrieve_body( $response );
		$response_code = wp_remote_retrieve_response_code( $response );

		$xml = simplexml_load_string( $response_body );

		if ( $xml && 200 === $response_code ) {

			return $xml;

		}

	}

	public function apartments_shortcode( $atts, $content = null ) {
		$a = shortcode_atts(
			array(
				'type'          => '',
				'show_reserved' => 'false',
				'company_name'  => '',
			),
			$atts
		);

		$filter_types = preg_replace( '/\s*,\s*/', ',', filter_var( $a['type'], FILTER_SANITIZE_STRING ) );
    	$types        = explode( ',', $filter_types );
		$hc_name      = sanitize_title( $a['company_name'] );

		$houses = $this->fetch_apartments( get_option( 'kouta-pdx-remote-url' ) );

		if ( $houses ) {

			$content = '<div class="pdx-houses">';

			ob_start();

			foreach ( $houses as $house ) {

				$status = (string) $house->Status;

				if ( 'Varattu' === $status && 'false' === $a['show_reserved'] ) {
					continue;
				}

				$attributes = $house->attributes();

				if ( ! empty( $a['type'] ) && ! in_array( $attributes->type, $types ) ) {
					continue;
				}

				$name = (string) sanitize_title( $house->HousingCompanyName );

				if ( ! empty( $hc_name ) && $name !== $hc_name ) {
					continue;
				}

				include KOUTA_PDX_BASE_DIR . '/public/partials/kouta-pdx-house.php';
			}

			$content .= ob_get_clean();
			$content .= '</div>';

			return $content;

		}

	}

	public function housing_companies_shortcode( $atts, $content = null ) {

		$a = shortcode_atts(
			array(
				'type' => '',
				'name' => '',
			),
			$atts
		);

		$filter_types = preg_replace( '/\s*,\s*/', ',', filter_var( $a['type'], FILTER_SANITIZE_STRING ) );
    	$types        = explode( ',', $filter_types );
		$hc_name      = sanitize_title( $a['name'] );

		$housing_company = $this->fetch_apartments( get_option( 'kouta-pdx-hc-remote-url' ) );

		if ( $housing_company ) {

			$content = '<div class="pdx-housing-companies">';

			ob_start();

			foreach ( $housing_company as $main ) {
				$type = (string) $main->apartment->types;
				$name = (string) sanitize_title( $main->name );

				if ( ! empty( $a['type'] ) && ! in_array( $type, $types, true ) ) {
					continue;
				}

				if ( ! empty( $hc_name ) && $name !== $hc_name ) {
					continue;
				}

				include KOUTA_PDX_BASE_DIR . '/public/partials/kouta-pdx-housing-company.php';
			}

			$content .= ob_get_clean();
			$content .= '</div>';

			return $content;

		}

	}

}
