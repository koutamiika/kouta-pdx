<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Pdx
 * @subpackage Kouta_Pdx/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Kouta_Pdx
 * @subpackage Kouta_Pdx/includes
 * @author     Kouta Media <miika@koutamedia.fi>
 */
class Kouta_Pdx_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
