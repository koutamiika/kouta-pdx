��          T      �       �      �       �   "   �   =     E   M  -   �  �  �     W     m  "   �  %   �  E   �                                             Check for updates There is no changelog available. Unknown update checker status "%s" the plugin titleA new version of the %s plugin is available. the plugin titleCould not determine if updates are available for %s. the plugin titleThe %s plugin is up to date. Project-Id-Version: plugin-update-checker
PO-Revision-Date: 2021-03-11 04:49+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_x
Last-Translator: 
Language: fi
X-Poedit-SearchPath-0: .
 Tarkista päivitykset Ei muutoslokia saatavilla. Tuntematon päivityksen tila: "%s" Uusi versio saatavilla lisäosaan %s. Ei voitu määrittää, onko päivityksiä saatavilla lisäosalle %s. Lisäosa %s on ajantasalla. 