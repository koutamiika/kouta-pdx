=== Kouta PDX ===
Donate link: https://koutamedia.fi
Tags: PDX, Oikotie
Requires at least: 5.6
Tested up to: 5.8
Stable tag: 1.3.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Tuo myytävät ja vuokrattavat kohteet kotisivuillesi PDX+ -järjestelmästä.

== Description ==

Tuo myytävät ja vuokrattavat kohteet kotisivuillesi PDX+ -järjestelmästä.

== Changelog ==

= 1.3.0 =
* Added company_name attribute to [pdx_apartments] shortcode.

= 1.2.0 =
* Added name attribute to [pdx_housing_companies] shortcode.
* Don't show "Lataa esite" link if it's empty.

= 1.1.0 =
* Added type attribute to [pdx_housing_companies] shortcode.

= 1.0.3 =
* Allow youtu.be url in magnific popup.

= 1.0.2 =
* [pdx_apartments] type-attribute empty by default.
* Minor styling fix.

= 1.0.1 =
* Fixed add_submenu_page() position argument.